import PropTypes from "prop-types";
import TodoItem from "./TodoItem";

function TodoList(props) {
  const { todos } = props;

  const displayList = todos.map((oneItem)=> 
    <TodoItem key={oneItem.id} title = {oneItem.title}  completed = {oneItem.completed}/>
)
       

  return (
    <div>
      <h1>My todo list ({todos.length} items):</h1>
      <ul>
        {displayList}
      </ul>
    </div>
  );
}

TodoList.propTypes = {
  todos: PropTypes.array.isRequired,
};

export default TodoList;
